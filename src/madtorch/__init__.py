#!/usr/bin/env python
"""Provides NumberList and FrequencyDistribution, classes for statistics.

NumberList holds a sequence of numbers, and defines several statistical
operations (mean, stdev, etc.) FrequencyDistribution holds a mapping from
items (not necessarily numbers) to counts, and defines operations such as
Shannon entropy and frequency normalization.
"""

from .optimizer import MAD

__author__ = "Alp Dener"
__copyright__ = "Copyright 2021-Present, Alp Dener",
__license__ = "BSD"
__version__ = "0.1"
__maintainer__ = "Alp Dener"
__email__ = "adener@anl.gov"
__status__ = "Alpha"