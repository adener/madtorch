import numpy as np
import math
import timeit
import sys
import gc
import os
import warnings
import torch
from torch import Tensor
from typing import Tuple, Optional

class MAD(torch.optim.Optimizer):
    r"""Implements the Multisecant Accelerated Descent algorithm.

    Originally proposed in `Error-tolerant Multisecant Method for Nonlinearly Constrained Optimization`_ for
    constrained optimization with noisy or inaccurate gradients. This implementation of MAD is intended to solve
    training problems for physics-informed neural networks (PINNs) where the training is done under physics-based
    constraints. However, if ``MAD.step()`` is called without a closure, it will also solve any unconstrained
    conventional training problem.

    MAD expects constrained training problems to be formulated as

    .. math::
        :nowrap:
        \begin{eqnarray}
            \underset{p}{\min} &\quad L(p), \\
            \text{subject to}  &\quad h(p) = 0, \\
                               &\quad g(p) \leq 0,
        \end{eqnarray}
    
    where :math:`p` are model parameters, :math:`L(p)' is the core loss function, and :math:`h(x)` and :math:`g(x)` are
    equality and inequality constraints on the model output, respectively. The closure tuple should contain
    ``(L, h, g)`` where either the equality constraints ``h`` or inequality constraints ``g`` can be ``None``, but not
    both. Given the loss and constraints, MAD internally assembles the Lagrangian function and triggers the
    backpropogation for the derivatives. The user should avoid calling ``loss.backward()`` before the training step.
    
    For unconstrained problems, call ``MAD.step()`` without a closure term and trigger ``loss.backward()`` as you
    would in a conventional training workflow.

    This implementation incorporates an adaptive learning rate based on
    `RMSprop <https://www.cs.toronto.edu/~tijmen/csc321/slides/lecture_slides_lec6.pdf>`_
    and `AMSgrad <https://openreview.net/forum?id=ryQu7f-RZ>`_.

    Args:
        params (iterable): iterable of parameters to optimize or dicts defining parameter groups
        lr (float, optional): learning rate (default: 1e-2)
        qmax (int, optional): length of history stored for past iterates and gradients (default: 20)
        alpha (float, optional): multisecant scaling (default: 0.1)
        beta (float, optional): regularization factor (default: 0.5)
        eps (float, optional): RMS zero safeguard (default: 1e-8)
        rcond (float, optional): relative tolerance for singular values in the least squares subproblem (default: 1e-6)
        use_lstsq (boolean, optional): if True, the least squares subproblem is always solved with numpy.lstsq() (default: False)
        adaptive (str, optional): choose between 'rmsgrad, 'amsgrad', 'norm', and None (default: 'amsgrad')
        safeguard (boolean, optional): if True, NaNs and Infs in the computed search direction are zeroed (default: False)

    .. _Error-tolerant Multisecant Method for Nonlinearly Constrained Optimization:
        https://arxiv.org/abs/1709.06985
    """
    def __init__(self, params, lr=0.01, qmax=10, alpha=0.5, beta=0.1, rho=0.9, tau=0.01, eps=np.finfo(np.float32).eps, 
                 rcond=1e-06, use_lstsq=False, adaptive='amsgrad', safeguard=False):
        if not 1 < qmax:
            raise ValueError("Invalid history storage, must be greater than 1: {}".format(qmax))
        if not 0.0 <= lr:
            raise ValueError("Invalid learning rate: {}".format(lr))
        if not 0.0 <= alpha <= 1.0:
            raise ValueError("Invalid multisecant scale: {}".format(alpha))
        if not 0.0 <= beta <= 1.0:
            raise ValueError("Invalid multisecant regularization factor: {}".format(beta))
        if not 0.0 <= rho <= 1.0:
            raise ValueError("Invalid step direction RMS decay rate: {}".format(rho))
        if not 0.0 <= tau <= 1.0:
            raise ValueError("Invalid fraction-to-the-boundary parameter: {}".format(tau))
        if not 0.0 < eps:
            raise ValueError("Invalid zero safeguard: {}".format(eps))
        if not 0.0 < rcond:
            raise ValueError("Invalid multisecant matrix condition tolerance: {}".format(rcond))
        if adaptive not in ['rmsgrad', 'amsgrad', 'norm', None]:
            raise ValueError("Invalid adaptive LR method: {}".format(adaptive))

        defaults = dict(lr=lr, qmax=qmax, alpha=alpha, beta=beta, rho=rho, tau=tau, eps=eps, rcond=rcond, 
                        use_lstsq=use_lstsq, adaptive=adaptive, safeguard=safeguard)
        super(MAD, self).__init__(params, defaults)
        self.glob = dict(
            steps = 0,
            dir_sq_avg = None,
            dir_sq_avg_max = None,
            max_pert_log = None,
            lstsq_fallbacks = 0,
            q = 0,
            Y = None,
            R = None,
            yprev = None,
            rprev = None,
            lamb_eq = None,
            lamb_ineq = None,
            slack1 = None,
            slack2 = None,
            mu = 1.0,
            init_ineq = None,
            device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        )

    def __getstate__(self):
        state = super(MAD, self).__getstate__()
        state.update({'glob': self.glob})
        return state

    def __setstate__(self, state):
        super(MAD, self).__setstate__(state)
        if 'glob' in state.keys():
            self.glob.update(state['glob'])
        else:
            warnings.warn(
                'Incomplete MAD state: dictionary appears to be from an older version or a different optimizer',
                RuntimeWarning)
        
    def _clone_primal_vec(self, zero=False, grad=False, cat=True):
        primal_vec = []
        for group in self.param_groups:
            for i, p in enumerate(group['params']):
                if p.requires_grad:
                    primal_vec.append(p.detach().clone().view(-1))
                    if zero:
                        primal_vec[-1][:] = 0.0
                    elif grad:
                        if p.grad is not None:
                            if p.grad.is_sparse:
                                primal_vec[-1][:] = p.grad.to_dense().view(-1)[:]
                            else:
                                primal_vec[-1][:] = p.grad.view(-1)[:]
                        else:
                            primal_vec[-1][:] = 0.0

        if cat:
            return torch.cat(primal_vec, 0)
        else:
            return primal_vec

    def _clone_dual_vec(self, cons, zero=False):
        cons_clone = cons.detach().clone()
        if zero:
            cons_clone[:] = 0.0
        return cons_clone

    def _clone_primal_dual_vec(self, cons_eq, cons_ineq, zero=False, grad=False):
        pdarr = self._clone_primal_vec(grad=grad, zero=zero, cat=False)
        if cons_eq is not None:
            if grad:
                pdarr.append(self._clone_dual_vec(cons_eq))
            else:
                pdarr.append(self._clone_dual_vec(self.glob['lamb_eq'], zero=zero))
        if cons_ineq is not None:
            if grad:
                pdarr.append(self._clone_dual_vec(cons_ineq + self.glob['slack1'] - self.glob['slack2']))
            else:
                pdarr.append(self._clone_dual_vec(self.glob['lamb_ineq'], zero=zero))
        return torch.cat(pdarr, 0)

    def _eval_lagrangian_and_grad(self, loss, cons_eq, cons_ineq):
        if loss is None and (cons_eq is not None or cons_ineq is not None):
            raise RuntimeError('Loss function must be provided in closure[0] for constrained problems!')
        else:
            if loss is not None:
                lagrangian = torch.zeros_like(loss).add(loss)

                if cons_eq is not None:
                    if self.glob['lamb_eq'] is None:
                        self.glob['lamb_eq'] = self._clone_dual_vec(cons_eq, zero=True)

                    # standard Lagrangian for equality constraints
                    lagrangian += self.glob['lamb_eq'].dot(cons_eq)

                if cons_ineq is not None:
                    if self.glob['lamb_ineq'] is None:
                        self.glob['lamb_ineq'] = self._clone_dual_vec(cons_ineq, zero=True)

                    # update penalty parameter to new value based on inequality constraints
                    if self.glob['init_ineq'] is None:
                        self.glob['init_ineq'] = cons_ineq.norm(2).item()
                        self.glob['mu'] = 1.0
                    else:
                        feas_ineq = cons_ineq + self.glob['slack1'] - self.glob['slack2']
                        self.glob['mu'] = feas_ineq.norm(2).item()/self.glob['init_ineq']

                    # "magic step" for slacks from eq 3.4 in (Curtis, 2011)
                    self.glob['slack1'] = self.glob['mu'] - 0.5*cons_ineq + 0.5*torch.sqrt(torch.pow(cons_ineq, 2.) + 4*self.glob['mu']**2)
                    self.glob['slack2'] = self.glob['mu'] + 0.5*cons_ineq + 0.5*torch.sqrt(torch.pow(cons_ineq, 2.) + 4*self.glob['mu']**2)

                    # this is a combined penalty-slack Lagrangian with log barrier terms for non-zero bounds on slacks
                    eps = self.defaults['eps']
                    lagrangian += self.glob['lamb_ineq'].dot(cons_ineq + self.glob['slack1'] - self.glob['slack2'])
                    lagrangian -= self.glob['mu']*torch.sum(torch.log(self.glob['slack1'].add(eps)) + torch.log(self.glob['slack2'].add(eps)))
                    lagrangian += torch.sum(self.glob['slack2'])
                    
                lagrangian.backward(retain_graph=True)
            else:
                lagrangian = None

        return lagrangian

    def _update_basis(self, cons_eq, cons_ineq):
        if self.glob['q'] == self.defaults['qmax']:
            # history is full so we need to discard the oldest column of the history tensor and shift the rest
            for j in range(self.glob['q']-1):
                self.glob['Y'][:, j] = self.glob['Y'][:, j+1]
                self.glob['R'][:, j] = self.glob['R'][:, j+1]
            self.glob['q'] -= 1

        # update the iterate difference history
        pnew = self._clone_primal_vec()
        ynew = self._clone_primal_dual_vec(cons_eq, cons_ineq)
        self.glob['Y'][:, self.glob['q']] = (ynew - self.glob['yprev'])[:]

        # update the gradient difference history
        beta = self.defaults['beta']
        rnew = self._clone_primal_dual_vec(cons_eq, cons_ineq, grad=True)
        self.glob['R'][:, self.glob['q']] = (rnew - self.glob['rprev'])[:]
        if beta > 0.0:
            self.glob['R'][:len(pnew), self.glob['q']] += beta * (pnew - self.glob['yprev'][:len(pnew)])

        # update previous iterate storage and clean up memory
        self.glob['yprev'][:] = ynew[:]
        self.glob['rprev'][:] = rnew[:]
        del pnew, ynew, rnew
        gc.collect()

        self.glob['q'] += 1

    def _solve_gamma(self, use_lstsq=False):
        if use_lstsq:
            # np.lstsq() is slow but reliable and stable, use it as a fallback
            rcond = self.defaults['rcond']
            gamma,_,_,_ = np.linalg.lstsq(self.glob['R'][:, :self.glob['q']].cpu().numpy(), self.glob['rprev'].cpu().numpy(), rcond=rcond)
            gamma = torch.from_numpy(gamma).to(self.glob['device'])
        else:
            # QR is fast but may require diagonal corrections
            RtR = torch.mm(self.glob['R'][:, :self.glob['q']].t().double(), self.glob['R'][:, :self.glob['q']].double())
            RtR_diag = torch.diagonal(RtR)
            mach_eps = self.defaults['eps']
            pert = 0.0
            ind_filter = []
            for i in range(len(RtR_diag)):
                if RtR_diag[i].item() < mach_eps:
                    ind_filter.append(False)
                    pert = max(pert, 10.*mach_eps - RtR_diag[i].item())
                else:
                    ind_filter.append(True)
            if pert > np.min((RtR_diag.cpu().numpy()[ind_filter])):
                print("    == Diagonal correction too large: {} ==".format(pert))
                self.glob['lstsq_fallbacks'] += 1
                gamma = self._solve_gamma(use_lstsq=True)
            else:
                if pert > 0.0:
                    self.glob['max_pert_log'] = max(self.glob['max_pert_log'], pert)
                    RtR += pert*torch.eye(self.glob['q']).double().to(self.glob['device'])
                rhs = torch.matmul(self.glob['R'][:, :self.glob['q']].t().double(), self.glob['rprev'].double())
                if self.glob['q'] == 1:
                    gamma = (rhs/RtR)[0]
                else:
                    try:
                        gamma = torch.cholesky_solve(rhs.unsqueeze(1), torch.cholesky(RtR)).squeeze(1)
                    except RuntimeError as err:
                        print("    == Cholesky failed despite diagonal correction: {} ==".format(pert))
                        self.glob['lstsq_fallbacks'] += 1
                        gamma = self._solve_gamma(use_lstsq=True)
            del RtR, RtR_diag
            gc.collect()

        return gamma

    @torch.no_grad()
    def _apply_primal_dual_step(self, ds):
        # prep the step direction with the RMSgrad/AMSGrad adaptive learning rate
        if self.defaults['adaptive'] in ['rmsgrad', 'amsgrad']:
            rho = self.defaults['rho']
            eps = self.defaults['eps']
            if self.glob['dir_sq_avg'] is None:
                self.glob['dir_sq_avg'] = torch.zeros_like(ds)
                self.glob['dir_sq_avg'].addcmul_(ds, ds, value=1.-rho)
                if self.defaults['adaptive'] == 'amsgrad':
                    self.glob['dir_sq_avg_max'] = self.glob['dir_sq_avg'].detach().clone()
                else:
                    self.glob['dir_sq_avg_max'] = self.glob['dir_sq_avg']
            else:
                self.glob['dir_sq_avg'].mul_(rho).addcmul_(ds, ds, value=1.-rho)
                if self.defaults['adaptive'] == 'amsgrad':
                    torch.maximum(self.glob['dir_sq_avg_max'], self.glob['dir_sq_avg'], out=self.glob['dir_sq_avg_max'])
                else:
                    self.glob['dir_sq_avg_max'] = self.glob['dir_sq_avg']
            direction = ds.div(self.glob['dir_sq_avg_max'].add(eps).sqrt_())
        elif self.defaults['adaptive'] == 'norm':
            direction = ds.div(ds.norm(2))
        else:
            direction = ds

        # safeguard the step against NaNs and Infs
        if self.defaults['safeguard']:
            direction = torch.nan_to_num(direction, nan=0.0, posinf=0.0, neginf=0.0)

        # loop over parameters and apply the step
        begin = 0
        for group in self.param_groups:
            lr = group['lr']
            for p in group['params']:
                if p.requires_grad:
                    end = begin + len(p.view(-1))
                    step = torch.reshape(direction[begin:end], p.shape)
                    begin = end
                    p.add_(step, alpha=lr)

        # we can take a simple scaled step for the equality multipliers
        if self.glob['lamb_eq'] is not None:
            self.glob['lamb_eq'].add_(direction[self.eq_ind:self.ineq_ind], alpha=self.param_groups[0]['lr'])

        # inequality multipliers need to be updated with a fraction-to-the-boundary rule
        if self.glob['lamb_ineq'] is not None:
            tau = self.defaults['tau']
            found = False
            y = self.glob['lamb_ineq']
            dy = direction[self.ineq_ind:]
            for beta in np.linspace(1., 0., 100):  # greedy search over possible step lengths, start big and shrink
                ystar = y + beta*dy
                if not torch.any(torch.lt(ystar, tau*y)).item() and not torch.any(torch.gt(ystar, 1.-tau*(1.-y))):
                    found = True
                    break
            if not found:
                raise RuntimeError('Could not find valid step length for inequality multiplier update...')
            else:
                self.glob['lamb_ineq'] = ystar
        
        del direction
        gc.collect()

    def step(self, closure: Tuple[Tensor, Tensor, Tensor] = (None, None, None)):
        # this computes L = f + y_e^Tc_e + y_i^T(c_i + s) - mu*log(s)
        loss = closure[0]
        cons_eq = closure[1]
        cons_ineq = closure[2]
        lagrangian = self._eval_lagrangian_and_grad(loss, cons_eq, cons_ineq)

        if self.glob['steps'] == 0:
            # there's no accumulated history in the first MAD step so we allocate
            opt_size = [0, 0, 0]
            for group in self.param_groups:
                for p in group['params']:
                    if p.requires_grad:
                        opt_size[0] += len(p.view(-1))
            if self.glob['lamb_eq'] is not None:
                opt_size[1] = len(self.glob['lamb_eq'])
            if self.glob['lamb_ineq'] is not None:
                opt_size[2] = len(self.glob['lamb_ineq'])
            self.glob['Y'] = torch.zeros(np.sum(opt_size), self.defaults['qmax']).to(self.glob['device'])
            self.glob['R'] = torch.zeros(np.sum(opt_size), self.defaults['qmax']).to(self.glob['device'])

            # compute indexing to be able to separate opt vars
            self.param_ind = 0
            self.eq_ind = self.param_ind + opt_size[0]
            self.ineq_ind = self.eq_ind + opt_size[1]
            del opt_size
            gc.collect()

            # clone and save current iterate for use in the next update
            self.glob['yprev'] = self._clone_primal_dual_vec(cons_eq, cons_ineq)
            self.glob['rprev'] = self._clone_primal_dual_vec(cons_eq, cons_ineq, grad=True)

            # first direction is a very conservative gradient descent step
            ds = -1e-3 * self.glob['rprev']
        else:
            # update S and Y histories and solve the least squares problem for gamma
            self._update_basis(cons_eq, cons_ineq)
            gamma = self._solve_gamma(use_lstsq=self.defaults['use_lstsq'])

            # assemble the total update vector (this includes both primal and dual updates in the same tensor)
            alpha = self.defaults['alpha']
            ds = -alpha*self.glob['rprev'].double() - torch.matmul(self.glob['Y'][:, :self.glob['q']].double() - alpha*self.glob['R'][:, :self.glob['q']].double(), gamma)

        # finally apply the update
        self._apply_primal_dual_step(ds)
        self.glob['steps'] += 1

        del ds
        gc.collect()
        return lagrangian