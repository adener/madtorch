#!/usr/bin/env python

from distutils.core import setup
from setuptools import setup, find_packages
import re

# get package metadata from __init__.py for consistency
verstr = "unknown"
authstr = "unknown"
emstr = "unknown"
try:
    metaline = open('src/madtorch/__init__.py', "rt").read()
except EnvironmentError:
    pass # Okay, there is no metadata file.
else:
    VSRE = r"^__version__ = ['\"]([^'\"]*)['\"]"
    AURE = r"^__author__ = ['\"]([^'\"]*)['\"]"
    EMRE = r"^__email__ = ['\"]([^'\"]*)['\"]"
    mo = re.search(VSRE, metaline, re.M)
    if mo:
        verstr = mo.group(1)
    else:
        raise RuntimeError("unable to find version in src/madtorch/__init__.py")
    mo = re.search(AURE, metaline, re.M)
    if mo:
        authstr = mo.group(1)
    else:
        raise RuntimeError("unable to findauthor in src/madtorch/__init__.py")
    mo = re.search(EMRE, metaline, re.M)
    if mo:
        emstr = mo.group(1)
    else:
        raise RuntimeError("unable to find email in src/madtorch/__init__.py")

setup(
    name='MADtorch',
    version=verstr,
    description='Multisecant Accelerated Descent training method for pyTorch',
    author=authstr,
    author_email=emstr,
    url='https://gitlab.com/adener/mad',
    package_dir={'':'src'},
    packages=find_packages(),
    install_requires=[
        'numpy>=1.18.1',
        'torch>=1.8.1'
    ],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: BSD License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.7",
        "Topic :: Scientific/Engineering :: Artificial Intelligence"
    ]
)