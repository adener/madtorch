# Multisecant Accelerated Descent for pyTorch

<img align="left" style="float:right; padding:20px;" width="210" height="210" src="assets/logo-hex.png"/>

Multisecant Accelerated Descent (MAD) is a constrained training method based on approximating the entire
Karush-Kuhn-Tucker system using the Secant equations simultaneously enforced on all past `q` iterates. Multisecant
class of methods are closely related to quasi-Newton methods and have been shown to be effective at optimizing systems
with inaccurate gradients (e.g. stochastic mini-batch gradients used in machine learning).

This pyTorch implementation of MAD incorporates an adaptive learning rate based on the RMSgrad and AMSgrad training
methods. Each new search direction is scaled with the root mean square (RMS) of a decaying average of past search
directions, with an option to use only the maximum of decaying averages to prevent step size increases.

MAD can solve unconstrained training problems the same way any other pyTorch optimizer would. However, in order to
solve constrained training problems, the user must provide closure tuple for `optimizer.step(closure)` where
`closure[0]` is a scalar tensor for the loss criterion (e.g. mean squared error), and `closure[1]` and `closure[2]` are
1-D tensors for the equality and inequality constraints, respectively. MAD assumes that the inequality constraints
are defined as "less than or equal to zero", `g(x) <= 0`.

### Dependencies
- Numpy 1.18.1
- PyTorch 1.8.1

### Example Usage

```python
import torch
from madtorch import MAD
```

Define your model, criterion and initialize the optimizer:

```python
model = net()  # user-defined neural network
criterion = torch.nn.MSELoss()
optimizer = MAD(model.parameters())
```

Solve unconstrained training problems same as any other pyTorch optimizer:

```python
for epoch in range(num_epochs):
    trainloader = shuffle_data()  # user function
    for data, targets in trainloader:
        output = model(data)
        loss = criterion(output, targets)
        loss.backward()
        optimizer.step()
```

Constrained training requires defining a closure tuple:

```python
for epoch in range(num_epochs):
    trainloader = shuffle_data()  # userfunction
    for data, targets in trainloader:
        output = model(data)
        loss = criterion(output, targets)
        eqcons, ineqcons = evaluate_constraints(data, output)  # user function
        closure = (loss, eqcons, ineqcons)  # do not backpropagate!
        optimizer.step(closure)
```

### References
* Dener A "Towards Constrained Optimization in Machine Learning: An Error-Tolerant Multisecant Method for Training PINNs" SIAM Computational Science and Engineering Conference, MS85, March 1, 2021. ([PDF Slides](https://alp.dener.me/files/slides/siam-cse21.pdf))
* Hicken, J. E., Meng P., and Dener A. "Error-tolerant multisecant method for nonlinearly constrained optimization." arXiv preprint [arXiv:1709.06985](https://arxiv.org/abs/1709.06985) (2017).
